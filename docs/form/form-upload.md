# 附件上传


:::tip
- 如果你想修改file文件,由于上传的file是只读文件，
- 必须复制新的file才可以修改名字，完后赋值到done函数里
- 如果不修改的话直接写done()即可,可以参考uploadBefore函数
- 阿里云和七牛云同理使用
- 你也可以直接单独配置到column中
::::

``` js
//使用上传附件需要引入axios
import axios from 'axios'
Vue.use(Avue,{axios})
```

## 类型
:::demo  `propsHttp`配置请求接口返回的数据结构`name`和`url`,`props`和字典用法一致配置存储时候的`label`和`value`,如果返回相对路径可以配置`home`为图片的主域名
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {
          video:'/i/movie.ogg',
          imgUrl:[
            { "label": "avue@226d5c1a_image.png", "value": "/images/logo-bg.jpg" },
            {"label": "avue@226d5c1a_video.png", "value": 'https://www.w3school.com.cn/i/movie.ogg'}
          ],
          imgUrl5:[
            { "label": "avue@226d5c1a_image.png", "value": "/images/logo-bg.jpg" },
            {"label": "avue@226d5c1a_video.png", "value": 'https://www.w3school.com.cn/i/movie.ogg'}
          ],
          imgUrl3:'/images/logo-bg.jpg'
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '附件上传',
            prop: 'imgUrl',
            type: 'upload',
            loadText: '附件上传中，请稍等',
            span: 24,
            propsHttp: {
              url:'url',
              name:'name',
              res: 'data'
            },
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '视频',
            prop: 'video',
            type: 'upload',
            accept:"video/mp4",
            propsHttp: {
              url:'url',
              name:'name',
              home: 'https://www.w3school.com.cn'
            },
            listType: 'picture-img',
            span: 24,
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '头像',
            prop: 'imgUrl3',
            type: 'upload',
            listType: 'picture-img',
            span: 24,
            propsHttp: {
              url:'url',
              name:'name',
              res: 'data'
            },
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '照片墙',
            prop: 'imgUrl4',
            type: 'upload',
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            propsHttp: {
              url:'url',
              name:'name',
              res: 'data'
            },
            action: '/imgupload'
          },
          {
            label: '拖拽上传',
            prop: 'imgUrl5',
            type: 'upload',
            span: 24,
            dragFile: true,
            propsHttp: {
              url:'url',
              name:'name',
              res: 'data'
            },
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '缩略图上传',
            prop: 'imgUrl6',
            type: 'upload',
            limit: 3,
            span: 24,
            propsHttp: {
              url:'url',
              name:'name',
              res: 'data'
            },
            listType: 'picture',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          }
        ]
      }
    }
  }
}
</script>


```
:::


## 执行方法
:::demo  提供了各种吩咐的回调函数和执行方法
```html
<avue-form :option="option" v-model="form" :upload-preview="uploadPreview" 
:upload-error="uploadError" :upload-exceed="uploadExceed" :upload-delete="uploadDelete" :upload-before="uploadBefore" :upload-after="uploadAfter"></avue-form>

<script>
export default{
  data() {
    return {
      form: {
          imgUrl:[
            { "label": "avue@226d5c1a_image.png", "value": "/images/logo-bg.jpg" },
            {"label": "avue@226d5c1a_video.png", "value": 'https://www.w3school.com.cn/i/movie.ogg'}
          ],
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '附件上传',
            prop: 'imgUrl',
            type: 'upload',
            listType: 'picture-card',
            loadText: '附件上传中，请稍等',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          }
        ]
      }
    }
  },
  methods: {
    uploadDelete(column,file) {
      console.log(column,file)
      return this.$confirm(`这里是自定义的，是否确定移除该选项？`);
    },
    uploadBefore(file, done, loading,column) {
      console.log(file,column)
      //如果你想修改file文件,由于上传的file是只读文件，必须复制新的file才可以修改名字，完后赋值到done函数里,如果不修改的话直接写done()即可
      var newFile = new File([file], '1234', { type: file.type });
      done(newFile)
      this.$message.success('上传前的方法')
    },
    uploadError(error, column) {
      this.$message.success('上传失败')
      console.log(error, column)
    },
    uploadAfter(res, done, loading,column) {
      console.log(res,column)
      done()
      this.$message.success('上传后的方法')
    },
    uploadPreview(file,column,done){
      console.log(file,column)
      done()//默认执行打开方法
      this.$message.success('自定义查看方法,查看控制台')
    },
    uploadExceed(limit, files, fileList, column){
      console.log(limit, files, fileList, column)
      this.$message.success('自定义查看方法,查看控制台')
    },
    submit() {
      this.$message.success('当前数据' + JSON.stringify(this.form))
    }
  }
}
</script>


```
:::

## 删除回调
:::demo  配置`onRemove`图片删除回调函数
```html
<avue-form :option="option" v-model="form"> 
</avue-form>

<script>
export default{
  data() {
    return {
      form: {
        imgUrl3:['/images/logo-bg.jpg'],
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '水印头像',
            prop: 'imgUrl3',
            type: 'upload',
            listType: 'picture-card',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            onRemove:(file,fileList)=>{
              this.$message.success('图片删除回调')
            },
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          }
        ]
      }
    }
  }
}
</script>


```
:::


## 传参
:::demo  可以配置`data`和`headers`属性作为传递参数
```html
<avue-form :option="option" v-model="form"> 
</avue-form>

<script>
export default{
  data() {
    return {
      form: {},
      option: {
        labelWidth: 120,
        column: [
          {
            label: '水印头像',
            prop: 'imgUrl3',
            type: 'upload',
            listType: 'picture-img',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            data:{
              a:1
            },
            headers:{
              b:1
            },
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          }
        ]
      }
    }
  }
}
</script>


```
:::



## 限制文件类型和大小数量
:::demo  可以配置`accept`和`limit`以及`fileSize`对应参数即可,`fileSize`对应基础单位为M
```html
<avue-form :option="option" v-model="form"> 
</avue-form>

<script>
export default{
  data() {
    return {
      form: {},
      option: {
        labelWidth: 120,
        column: [
          {
            label: '水印头像',
            prop: 'imgUrl3',
            type: 'upload',
            listType: 'picture-card',
            accept:'image/png, image/jpeg',
            limit:2,
            fileSize:10,
            span: 24,
            propsHttp: {
              res: 'data'
            },
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          }
        ]
      }
    }
  }
}
</script>


```
:::


## 自定义卡槽
:::demo  列名的`prop`加上`Type`即可自定义内容
```html
<avue-form :option="option" v-model="form"> 
  <template slot="imgUrlType" slot-scope="{file}">
    <span>自定义卡槽{{file}}</span>
  </template>
</avue-form>

<script>
export default{
  data() {
    return {
      form: {
          imgUrl:[
            { "label": "avue@226d5c1a_image.png", "value": "/images/logo-bg.jpg" },
            {"label": "avue@226d5c1a_video.png", "value": 'https://www.w3school.com.cn/i/movie.ogg'}
          ],
      },
      option: {
        labelWidth: 120,
        column: [
          
          {
            label: '附件上传',
            prop: 'imgUrl',
            type: 'upload',
            loadText: '附件上传中，请稍等',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          }]
      }
    }
  }
}
</script>


```
:::




## 数据类型
:::demo  提供了`dataType`参数可以去配置存到数据库的结构体类型,也可配`props`和`propsHttp`属性去加载不用的结构体中的`key`和`value`
```html
<avue-form :option="option" v-model="form" > 
</avue-form>

<script>
export default{
  data() {
    return {
      form: {
          string:'/images/logo-bg.jpg,/images/logo-bg.jpg',
          img:['/images/logo-bg.jpg','/images/logo-bg.jpg','./xx/xx.sql']
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '数组图片组',
            prop: 'img',
            dataType: 'array',
            type: 'upload',
            propsHttp: {
              res: 'data.0'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          },
          {
            label: '字符串图片组',
            prop: 'string',
            dataType: 'string',
            type: 'upload',
            propsHttp: {
              res: 'data'
            },
            span: 24,
            listType: 'picture-card',
            tip: '只能上传jpg/png文件，且不超过500kb',
            action: '/imgupload'
          }
        ]
      }
    }
  }
}
</script>


```
:::


## 图片水印
:::demo  可以配置`canvasOption`属性去生成水印和压缩图片，
```html
<avue-form :option="option" v-model="form"> 
</avue-form>

<script>
export default{
  data() {
    return {
      form: {
          imgUrl3:'/images/logo-bg.jpg',
      },
      option: {
        labelWidth: 120,
        column: [
          {
            label: '水印头像',
            prop: 'imgUrl3',
            type: 'upload',
            listType: 'picture-img',
            span: 24,
            propsHttp: {
              res: 'data'
            },
            canvasOption: {
              text: 'avue',
              ratio: 0.1
            },
            tip: '只能上传jpg/png用户头像，且不超过500kb',
            action: '/imgupload'
          }
        ]
      }
    }
  }
}
</script>


```
:::


## 拖拽排序



``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://cdn.staticfile.org/Sortable/1.10.0-rc2/Sortable.min.js"></script>
```
:::tip
 2.8.27+
::::

:::demo 配置`drag`属性即可开启拖拽排序
```html
{{form}}
<avue-form :option="option" v-model="form"> </avue-form>
<script>
export default {
   data(){
     return {
       form:{
          img:['/images/logo-bg.jpg','/images/logo-bg.jpg','./xx/xx.sql']
       },
       option: {
          labelWidth: 120,
          column: [
            {
              label: '数组图片组',
              prop: 'img',
              drag:true,
              dataType: 'array',
              type: 'upload',
              propsHttp: {
                res: 'data.0'
              },
              span: 24,
              listType: 'picture-card',
              tip: '只能上传jpg/png文件，且不超过500kb',
              action: '/imgupload'
            }
          ]
        }
     }
   }
}
</script>

```
:::


## 阿里云oss上传



``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://cdn.staticfile.org/ali-oss/6.10.0/aliyun-oss-sdk.min.js"></script>
```
```js
//入口处全局配置阿里云的参数
Vue.use(AVUE, {
  ali: {
    region: 'oss-cn-beijing',
    endpoint: 'oss-cn-beijing.aliyuncs.com',
    stsToken:'',
    accessKeyId: '',
    accessKeySecret: '',
    bucket: 'avue',
  }
})
```

:::tip
 1.0.6+
::::


:::demo 
```html
<avue-form :option="option" v-model="form"> </avue-form>
<script>
export default {
   data(){
     return {
       form:{},
       option: {
          labelWidth: 120,
          column: [
            {
              label: '阿里上传',
              prop: 'imgUrl',
              type: 'upload',
              listType: 'picture-img',
              canvasOption: {
                text: 'avue',
                ratio: 0.1
              },
              oss: 'ali',
              loadText: '附件上传中，请稍等',
              span: 24,
              tip: '只能上传jpg/png文件，且不超过500kb',
            }
          ]
        }
     }
   }
}
</script>

```
:::



## 七牛云oss上传



``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://avuejs.com/cdn/CryptoJS.js"></script>
```
- 华东——http(s)://upload.qiniup.com
- 华北——http(s)://upload-z1.qiniup.com
- 华南——http(s)://upload-z2.qiniup.com
- 北美——http(s)://upload-na0.qiniup.com
- 东南亚——http(s)://upload-as0.qiniup.com

```js
//入口处全局配置七牛云的参数 
Vue.use(window.AVUE, {
  qiniu: {
    AK: '',//七牛云相关密钥
    SK: '',//七牛云相关密钥
    bucket:'https://upload.qiniup.com'//存储地区，默认为华东，其他的如下表
    scope: 'test',//存储空间名称
    url: 'https://cdn.avuejs.com/'//外链的域名地址
  }
})
```

:::tip
 1.0.6+
::::


:::demo 
```html
<avue-form :option="option" v-model="form"> </avue-form>
<script>
export default {
   data(){
     return {
       form:{},
       option: {
          labelWidth: 120,
          column: [
            {
              label: '七牛上传',
              prop: 'imgUrl',
              type: 'upload',
              listType: 'picture-img',
              propsHttp: {
                name: 'hash',
                url: "key"
              },
              oss: 'qiniu',
              loadText: '附件上传中，请稍等',
              span: 24,
              tip: '只能上传jpg/png文件，且不超过500kb',
            }
          ]
        }
     }
   }
}
</script>

```
:::




