# 标题字段宽度


:::demo `widthLabel`为标题的宽度，默认为`110`，可以配置到`option`下作用于全部,也可以单独配置每一项
```html
<avue-form v-model="form" :option="option"></avue-form>

<script>
export default {
    data() {
      return {
        form: {
            name:'张三',
            sex:'男'
          },
        option:{
          labelWidth:150,
          column:[
             {
              labelWidth:30,
              label:'姓名',
              prop:'name'

            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      }
    }
}
</script>
```
:::