# 树型选择框

:::tip
 2.5.3+
::::

## 普通用法

:::demo  
```html
<avue-form v-model="form" :option="option" ></avue-form>

<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
const DIC=  [
      {
        label: '一级1',
        value: 1,
        children:[{
            label: '子类',
            value: 100,
        }]
      }, {
        label: '一级2',
        value: 2,
      },{
        label: '一级3',
        value: 3,
      }, {
        label: '一级4',
        value: 4,
      },{
        label: '一级5',
        value: 5,
      }, {
        label: '一级6',
        value: 6,
      },{
        label: '一级7',
        value: 7,
      }, {
        label: '一级8',
        value: 8,
      }, {
        label: '一级9',
        value: 9,
      }, {
        label: '一级10',
        value: 10,
      }
    ]
export default {
    data() {
      return {
        form:{
          tree1:1,
          tree2:[1,2]
        },
        option:{
          column: [{
              label: '单选',
              prop: 'tree1',
              filter:false,
              type: 'tree',
              dicData:DIC
          }, {
              label: '多选',
              prop: 'tree2',
              type: 'tree',
              filterText:'搜索关键字制自定义',
              multiple:true,
              dicData:DIC
          }]
        }
      };
    }
}
</script>
```
:::



## 节点事件

:::demo  
```html
<avue-form v-model="form" :option="option" ></avue-form>

<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
const DIC=  [
      {
        label: '一级1',
        value: 1,
        children:[{
            label: '子类',
            value: 100,
        }]
      }, {
        label: '一级2',
        value: 2,
      },{
        label: '一级3',
        value: 3,
      }, {
        label: '一级4',
        value: 4,
      },{
        label: '一级5',
        value: 5,
      }, {
        label: '一级6',
        value: 6,
      },{
        label: '一级7',
        value: 7,
      }, {
        label: '一级8',
        value: 8,
      }, {
        label: '一级9',
        value: 9,
      }, {
        label: '一级10',
        value: 10,
      }
    ]
export default {
    data() {
      return {
        form:{
          tree:[1,2]
        },
        option:{
          column: [{
              label: '多选',
              prop: 'tree',
              type: 'tree',
              multiple:true,
              nodeClick:(data, node, nodeComp)=>{
                this.$message.success(JSON.stringify(data))
              },
              checked:(checkedNodes, checkedKeys, halfCheckedNodes, halfCheckedKeys)=>{
                this.$message.success(JSON.stringify(checkedKeys))
              },
              dicData:DIC
          }]
        }
      };
    }
}
</script>
```
:::


## 合并展示

:::demo  
```html
<avue-form v-model="form" :option="option" ></avue-form>

<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
const DIC=  [
      {
        label: '一级1',
        value: 1,
        children:[{
            label: '子类',
            value: 100,
        }]
      }, {
        label: '一级2',
        value: 2,
      },{
        label: '一级3',
        value: 3,
      }, {
        label: '一级4',
        value: 4,
      },{
        label: '一级5',
        value: 5,
      }, {
        label: '一级6',
        value: 6,
      },{
        label: '一级7',
        value: 7,
      }, {
        label: '一级8',
        value: 8,
      }, {
        label: '一级9',
        value: 9,
      }, {
        label: '一级10',
        value: 10,
      }
    ]
export default {
    data() {
      return {
        form:{
          tree2:[1,2]
        },
        option:{
          column: [{
              label: '多选',
              prop: 'tree2',
              type: 'tree',
              tags:true,
              filterText:'搜索关键字制自定义',
              multiple:true,
              dicData:DIC
          }]
        }
      };
    }
}
</script>
```
:::





## 自定义节点内容
可以自定义下拉框的显示格式和回显的格式
:::tip
 2.1.0+
::::

:::demo  配置`typeslot`卡槽开启即可自定义下拉框的内容,`typeformat`配置回显的内容,但是你提交的值还是`value`并不会改变
```html
<avue-form :option="option" v-model="form">
  <template slot="codeType" slot-scope="{item,value,label}">
      <span>{{ item }}</span>
  </template>
  <template slot="codesType" slot-scope="{item,value,label}">
      <span>{{ item }}</span>
  </template>
</avue-form>
<script>
var baseUrl = 'https://cli.avuejs.com/api/area'
export default {
    data() {
      return {
        form: {},
        option: {
          labelWidth: 100,
          column: [{
              label: '单选',
              prop: 'code',
              type: 'tree',
              props: {
                label: 'name',
                value: 'code'
              },
              typeslot: true,
              dicUrl: `${baseUrl}/getProvince`,
              typeformat(item, label, value) {
                return `值:${item[label]}-名:${item[value]}`
              },
              rules: [
                {
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }
              ]
            }, {
              label: '多选',
              prop: 'codes',
              type: 'tree',
              props: {
                label: 'name',
                value: 'code'
              },
              multiple: true,
              typeslot: true,
              dicUrl: `${baseUrl}/getProvince`,
              typeformat(item, label, value) {
                return `值:${item[label]}-名:${item[value]}`
              },
              rules: [
                {
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }
              ]
            }
          ]
        }
      }
    }
}
</script>

```
:::


## 辅助语
:::tip
 2.8.24+
::::

:::demo  
```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
const DIC=  [
      {
        label: '一级1',
        desc:'辅助语',
        value: 1,
        children:[{
            label: '子类',
            desc:'辅助语',
            value: 100,
        }]
      }, {
        label: '一级2',
        desc:'辅助语',
        value: 2,
      },{
        label: '一级3',
        value: 3,
      }, {
        label: '一级4',
        value: 4,
      },{
        label: '一级5',
        value: 5,
      }, {
        label: '一级6',
        value: 6,
      },{
        label: '一级7',
        value: 7,
      }, {
        label: '一级8',
        value: 8,
      }, {
        label: '一级9',
        value: 9,
      }, {
        label: '一级10',
        value: 10,
      }
    ]
export default {
    data() {
      return {
        form: {},
        option: {
          labelWidth: 100,
          column: [{
              label: '单选',
              prop: 'code',
              type: 'tree',
              dicData:DIC
            }
          ]
        }
      }
    }
}
</script>

```
:::

## 懒加载
:::tip
 2.5.3+
::::

:::demo  
```html
<avue-form v-model="form" :option="option" ></avue-form>

<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
const DIC=  [
      {
        label: '一级1',
        value: 1,
        children:[{
            label: '子类',
            value: 100,
        }]
      }, {
        label: '一级2',
        value: 2,
      },{
        label: '一级3',
        value: 3,
      }, {
        label: '一级4',
        value: 4,
      },{
        label: '一级5',
        value: 5,
      }, {
        label: '一级6',
        value: 6,
      },{
        label: '一级7',
        value: 7,
      }, {
        label: '一级8',
        value: 8,
      }, {
        label: '一级9',
        value: 9,
      }, {
        label: '一级10',
        value: 10,
      }
    ]
export default {
    data() {
      return {
        form:{
          id:'110000'
        },
        option:{
          column: [{
            label:'懒加载',
            prop:'id',
            type:'tree',
            dicUrl:`${baseUrl}/getProvince?id={{key}}`,
            props: {
              label: 'name',
              value: 'code'
            },
            lazy:true,
            treeLoad: (node, resolve)=> {
              let stop_level = 2;
              let level = node.level;
              let data = node.data || {}
              let code = data.code;
              let list = [];
              let callback = () => {
                resolve((list || []).map(ele => {
                  return Object.assign(ele, {
                    leaf: level >= stop_level
                  })
                }));
              }
              if (level == 0) {
                axios.get(`${baseUrl}/getProvince`).then(res => {
                  list = res.data.data;
                  callback()
                })
              }
              if (level == 1) {
                axios.get(`${baseUrl}/getCity/${code}`).then(res => {
                  list = res.data.data;
                  callback()
                })
              } else if (level == 2) {
                axios.get(`${baseUrl}/getArea/${code}`).then(res => {
                  list = res.data.data;
                  callback()
                })
              }else{
                list=[]
                callback()
              }
            }
          }]
        }
      };
    }
}
</script>
```
:::

