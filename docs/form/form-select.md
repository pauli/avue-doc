# 选择框
## 普通用法
:::demo 可以配置`dicData`和`dicUrl`属性来实现本地字典和网络字典

```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
export default {
  data(){
    return {
       form:{},
       option:{
          column: [{
            label: '本地字典',
            prop: 'select1',
            type: 'select',
            dicData:[{
              label:'字典1',
              value:0
            },{
              label:'字典2',
              value:1
            }]
          },{
            label: '网络字典',
            prop: 'select2',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            dicUrl: 'https://cli.avuejs.com/api/area/getProvince'
          }]
       }
    }
  }
}
</script>

```
:::


## 自定义节点内容

:::tip
 2.1.0+
::::

:::demo  配置`typeslot`卡槽开启即可自定义下拉框的内容,`typeformat`配置回显的内容,但是你提交的值还是`value`并不会改变
```html
<avue-form :option="option" v-model="form">
 <template slot="provinceType" slot-scope="{item,value,label}">
      <span>{{ item }}</span>
  </template>
</avue-form>
<script>
export default {
    data() {
      return {
        form: {
          province:'120000'
        },
        option: {
          column: [
            {
              label: '单选',
              prop: 'province',
              type: 'select',
              props: {
                label: 'name',
                value: 'code'
              },
              typeslot: true,
              dicUrl: 'https://cli.avuejs.com/api/area/getProvince',
              typeformat(item, label, value) {
                return `值:${item[label]}-名:${item[value]}`
              },
              rules: [{
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }]
             }]
          }
      }
    }
}
</script>

```
:::



## 多级联动
:::tip
 2.9.0+
::::

:::demo  `cascader`为需要联动的子选择框`prop`值，填写多个就会形成一对多的关系

```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
var baseUrl = 'https://cli.avuejs.com/api/area'
export default {
  data(){
    return {
       form:{
          province: '110000',
          city: '110100',
          area: '110101'
       },
       option:{
          column: [{
            label: '省份',
            prop: 'province',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            cascader: ['city'],
            dicUrl: `${baseUrl}/getProvince`,
            rules: [
              {
                required: true,
                message: '请选择省份',
                trigger: 'blur'
              }
            ]
          },{
            label:'文本框1',
            prop:'text1'
          },
          {
            label: '城市',
            prop: 'city',
            type: 'select',
            cascader:['area'],
            props: {
              label: 'name',
              value: 'code'
            },
            row: true,
            dicUrl: `${baseUrl}/getCity/{{key}}?province={{province}}`,
            rules: [
              {
                required: true,
                message: '请选择城市',
                trigger: 'blur'
              }
            ]
          },{
            label:'文本框2',
            prop:'text2'
          },{
            label:'文本框3',
            prop:'text3'
          },
          {
            label: '地区',
            prop: 'area',
            type: 'select',
            props: {
              label: 'name',
              value: 'code'
            },
            dicUrl: `${baseUrl}/getArea/{{key}}?city={{city}}`,
            rules: [
              {
                required: true,
                message: '请选择地区',
                trigger: 'blur'
              }
            ]
          }]
       }
    }
  }
}
</script>


```
:::


## 远程搜索
当你的下拉框数据量很大的时候，你可以启动远程搜索

:::demo  配置`remote`为`true`即可开启远程搜索，其中`dicUrl`中`{{key}｝`为用户输入的关键字,其他字典用法和参数保持不变,由于没有真实的接口，看控制台的network
```html
<avue-form :option="option" v-model="form" @submit="handleSubmit"></avue-form>
<script>
var baseUrl = 'https://cli.avuejs.com/api/area'
export default {
  data(){
    return {
       form:{
        province: '110000',
        province1: '110000,120000,130000,140000',
       },
       option: {
        column: [
          {
            label: '省份单选',
            prop: 'province',
            type: 'select',
            remote: true,
            props: {
              label: 'name',
              value: 'code'
            },
            dicUrl: `${baseUrl}/getProvince?id={{key}}`,
            rules: [
              {
                required: true,
                message: '请选择省份',
                trigger: 'blur'
              }
            ]
          },{
            label: '省份多选',
            prop: 'province1',
            type: 'select',
            multiple:true,
            remote: true,
            props: {
              label: 'name',
              value: 'code'
            },
            dicUrl: `${baseUrl}/getProvince?id={{key}}`,
            rules: [
              {
                required: true,
                message: '请选择省份',
                trigger: 'blur'
              }
            ]
          }
        ]
      }
    }
  },
  methods:{
    handleSubmit(form){
       this.$message.success(JSON.stringify(this.form))
    }
  }
}
</script>

```
:::


## 分组

:::tip
 2.1.0+
::::

:::demo  配置`group`为`true`即可开启分组模式
```html
<avue-form :option="option" v-model="obj"></avue-form>
<script>
export default {
    data() {
      return {
        obj: {
         select:'Shanghai'
        },
        option: {
          column: [
            {
            label: '分组',
            prop: 'select',
            type: 'select',
            group: true,
            dicData: [{
              label: '热门城市',
              groups: [{
                value: 'Shanghai',
                label: '上海',
                desc:'描述'
              }, {
                value: 'Beijing',
                label: '北京'
              }]
            }, {
              label: '城市名',
              groups: [{
                value: 'Chengdu',
                label: '成都'
              }, {
                value: 'Shenzhen',
                label: '深圳'
              }, {
                value: 'Guangzhou',
                label: '广州'
              }, {
                value: 'Dalian',
                label: '大连'
              }]
            }]
            }
          ]
        }
      }
    }
}
</script>

```
:::


## 拖拽

:::tip
 2.1.0+
::::


``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<script src="https://cdn.staticfile.org/Sortable/1.10.0-rc2/Sortable.min.js"></script>
```


:::demo  配置`drag`为`true`即可开启拖拽模式
```html
<avue-form :option="option" v-model="obj"></avue-form>
<script>
export default {
    data() {
      return {
        obj: {
         select:['Shanghai','Beijing','Shenzhen']
        },
        option: {
          column: [
            {
            label: '拖拽',
            prop: 'select',
            type: 'select',
            drag: true,
            multiple: true,
            dicData: [{
                value: 'Shanghai',
                label: '上海'
              }, {
                value: 'Beijing',
                label: '北京'
              }, {
                value: 'Shenzhen',
                label: '深圳'
              }]
            }
          ]
        }
      }
    }
}
</script>

```
:::


