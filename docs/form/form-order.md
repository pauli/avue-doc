# 字段排序
:::tip
2.6.16+
::::

## 普通用法

:::demo  设`order`属性可与排序表单字段
```html
<avue-form v-model="form" :option="option" ></avue-form>

<script>
export default {
    data() {
      return {
        form:{
          name:'张三',
          sex:'男'
        },
        option:{
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex',
              order:1
            }
          ]
        },
      };
    },
    methods: {
    }
}
</script>
```
:::