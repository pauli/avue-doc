
# 输入框



## 普通用法

:::demo
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {},
      option: {
        column: [
         {
            label:'单文本框',
            prop:'input',
            row:true
         }, {
            label:'密码框',
            prop:'password',
            type:'password',
            row:true
         }, {
            label:'多文本框',
            prop:'textarea',
            type:'textarea'
         }
        ]
      }
    }
  }
}
</script>

```
:::


## 复合型输入框

:::demo 可以通过 `prefixIcon` 和 `suffixIcon`以及`prepend`和`append`属性在 `input` 组件首部和尾部增加显示图标
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {},
      option: {
        column: [
         {
            label:'单文本框1',
            prop:'input',
            prepend:'HTTP',
            append:'COM',
            row:true
         },
         {
            label:'单文本框2',
            prop:'input',
            suffixIcon:"el-icon-date",
            prefixIcon:"el-icon-search"
         }
        ]
      }
    }
  }
}
</script>

```
:::


## 多行文本框

:::demo 通过设置 `minRows`和`minRows` 属性可以使得文本域的高度能够根据文本内容自动进行调整
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {},
      option: {
        column: [
         {
            label:'多行文本框',
            prop:'input',
            type:'textarea',
            minRows: 8, 
            maxRows: 10
         }
        ]
      }
    }
  }
}
</script>

```
:::

## 字符长度提示

:::demo  配置`showWordLimit`为`true`即可
```html
<avue-form :option="option" v-model="form"></avue-form>

<script>
export default{
  data() {
    return {
      form: {},
      option: {
        column: [
         {
            label:'单文本框',
            prop:'input',
            maxlength:10,
            showWordLimit:true
         }, {
            label:'多文本框',
            prop:'textarea',
            type:'textarea',
            minRows:10,
            maxlength:20,
            span:24,
            showWordLimit:true
         }
        ]
      }
    }
  }
}
</script>

```
:::
