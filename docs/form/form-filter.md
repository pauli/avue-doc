# 数据过滤
:::tip
 2.9.3
::::

:::demo  `filterDic`设置为`true`返回的对象不会包含`$`前缀的字典翻译, `filterNull`设置为`true`返回的对象不会包含空数据的字段
```html
<el-button size="small" type="danger" @click="filterDic">过滤数据字典</el-button>
<el-button size="small" type="success" @click="filterNull">过滤空数据</el-button>
<el-button size="small" type="primary" @click="filter">不过滤</el-button>
<p>{{form}}</p>
<avue-form :key="reload" v-model="form" :option="option" ></avue-form>

<script>
export default {
    data() {
      return {
        reload:Math.random(),
        form: {
          cascader:[0,1],
          tree:0
        },
        option:{
          column:[{
            label:'姓名',
            prop:'name',
          },{
              label:'级别',
              prop:'cascader',
              type:'cascader',
              dicData:[{
                label:'一级',
                value:0,
                children:[
                  {
                    label:'一级1',
                    value:1,
                  },{
                    label:'一级2',
                    value:2,
                  }
                ]
              }],
            },
            {
              label:'树型',
              prop:'tree',
              type:'tree',
              dicData:[{
                label:'一级',
                value:0,
                children:[
                  {
                    label:'一级1',
                    value:1,
                  },{
                    label:'一级2',
                    value:2,
                  }
                ]
              }]
            }]
        }
      }
    },
    methods:{
      refresh(){
        this.reload=Math.random()
      },
      filter(){
        this.option.filterDic=false
        this.option.filterNull=false
        this.refresh()
      },
      filterDic(){
        this.option.filterDic=true
        this.refresh()
      },
      filterNull(){
        this.option.filterNull=true
        this.refresh()
      }
    }
}
</script>
```
:::


## 清空过滤字段
:::tip
 2.9.3
::::

:::demo  `filterParams`为点击清空按钮，表单不会清除的数据，默认主键`rowKey`是不会清空的，默认为`id`
```html
<avue-form  v-model="form" :option="option" ></avue-form>

<script>
export default {
    data() {
      return {
        form:{
          name:'smallwei',
          cascader:[0,1],
          tree:0
        },
        option:{
          filterParams:['name'],
          column:[{
            label:'姓名',
            prop:'name',
          },{
              label:'级别',
              prop:'cascader',
              type:'cascader',
              dicData:[{
                label:'一级',
                value:0,
                children:[
                  {
                    label:'一级1',
                    value:1,
                  },{
                    label:'一级2',
                    value:2,
                  }
                ]
              }],
            },
            {
              label:'树型',
              prop:'tree',
              type:'tree',
              dicData:[{
                label:'一级',
                value:0,
                children:[
                  {
                    label:'一级1',
                    value:1,
                  },{
                    label:'一级2',
                    value:2,
                  }
                ]
              }]
            }]
        }
      }
    }
}
</script>
```
:::

