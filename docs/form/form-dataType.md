# 数据类型
:::tip
 1.0.11+
::::

## 普通用法 
:::demo  有些数据是数组形式的，我们传入字符串是无法识别的，解决数据字典和字段类型不匹配问题,配置`dataType`属性(string / number)
```html
{{obj}}
<avue-form :option="option" v-model="obj" > </avue-form>

<script>
let baseUrl = 'https://cli.avuejs.com/api/area'
export default{
  data() {
      return {
        obj: {
          cascader:'1,2',
          province: 120000,
          radio:1,
          checkbox:'1,2,3',
          selects:'1,2,3'
        },
        option: {
          column: [
            {
              label:'单选',
              prop:'radio',
              type:'radio',
              dataType:'number',
              dicData:[{
                label:'选项1',
                value:1
              },{
                label:'选项2',
                value:2
              },{
                label:'选项3',
                value:3
              }]
            },
            {
              label:'多选',
              prop:'checkbox',
              type:'checkbox',
              dataType:'number',
              dicData:[{
                label:'选项1',
                value:1
              },{
                label:'选项2',
                value:2
              },{
                label:'选项3',
                value:3
              }]
            },
            {
              label:'多选',
              prop:'selects',
              type:'select',
              multiple:true,
              dicData:[{
                label:'选项1',
                value:'1'
              },{
                label:'选项2',
                value:'2'
              },{
                label:'选项3',
                value:'3'
              }]
            },
            {
              label: '级联',
              prop: 'cascader',
              type: 'cascader',
              dataType:'string',
              dicData:[{
                label:'级别1',
                value:1,
                children:[{
                  label:'级别2',
                  value:2
                }]
              }],
              rules: [
                {
                  required: true,
                  message: '请选择级联',
                  trigger: 'blur'
                }
              ]
            },
            {
              label: '省份',
              prop: 'province',
              type: 'select',
              dataType:'number',
              props: {
                label: 'name',
                value: 'code'
              },
              dicUrl: `${baseUrl}/getProvince`,
              rules: [
                {
                  required: true,
                  message: '请选择省份',
                  trigger: 'blur'
                }
              ]
            }
          ]
        }
      }
    },
}
</script>

```
:::
