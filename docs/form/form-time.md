# 时间


## 普通用法

:::demo 
```html
<avue-form v-model="form" :option="option"></avue-form>

<script>
export default {
    data() {
      return {
        form:{},
        option:{
          column: [{
              label: "时间",
              prop: "time",
              type: "time",
          }]
        },
      };
    }
}
</script>

```
:::


## 格式化

:::demo  
```html
<avue-form v-model="form" :option="option"></avue-form>

<script>
export default {
    data() {
      return {
        form:{
           "time": "2021-08-02 12:00:00"
        },
        option:{
          column: [{
              label: "时间",
              prop: "time",
              type: "time",
              format:'HH时mm分ss秒',
              valueFormat:'HH:mm:ss'
          }]
        },
      };
    }
}
</script>
```
:::

## 时间范围

:::demo  
```html
<avue-form v-model="form" :option="option"></avue-form>

<script>
export default {
    data() {
      return {
        form:{},
        option:{
          labelWidth:110,
          column: [{
              label: "时间范围",
              prop:'timerange',
              type: 'timerange',
              format:'HH:mm:ss',
              valueFormat:'HH:mm:ss',
              startPlaceholder: '时间开始范围自定义',
              endPlaceholder: '时间结束范围自定义',
          }]
        },
      };
    }
}
</script>

```
:::
