# 多选

:::tip
 2.1.0+
::::


## 普通用法
:::demo  
```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
export default {
    data() {
      return {
        form: {
          checkbox:[0,1]
        },
        option: {
          column: [{
              label: '多选',
              prop: 'checkbox',
              type: 'checkbox',
              span:24,
              dicData:[{
                label:'男',
                value:0
              },{
                label:'女',
                value:1
              },{
                label:'未知',
                value:''
              }]
            }
          ]
        }
      }
    }
}
</script>

```
:::


## 全选
:::demo  配置`all`为`true`
```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
export default {
    data() {
      return {
        form: {
          checkbox:[0,1]
        },
        option: {
          column: [{
              label: '多选',
              prop: 'checkbox',
              type: 'checkbox',
              all:true,
              span:24,
              dicData:[{
                label:'男',
                value:0
              },{
                label:'女',
                value:1
              },{
                label:'未知',
                value:''
              }]
            }
          ]
        }
      }
    }
}
</script>

```
:::



## 数量限制
:::demo  使用`min`和`max`属性能够限制可以被勾选的项目的数量。
```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
export default {
    data() {
      return {
        form: {
          checkbox:[0]
        },
        option: {
          column: [{
              label: '空心多选',
              prop: 'checkbox',
              span:24,
              type: 'checkbox',
              border:true,
              min:1,
              max:2,
              dicData:[{
                label:'男',
                value:0
              },{
                label:'女',
                value:1
              },{
                label:'未知',
                value:''
              }]
            }
          ]
        }
      }
    }
}
</script>

```
:::




## 按钮样式
:::demo  配置`button`为`true`
```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
export default {
    data() {
      return {
        form: {
          checkbox:[0,1],
        },
        option: {
          column: [{
              label: '实心多选',
              prop: 'checkbox',
              type: 'checkbox',
              span:24,
              button:true,
              dicData:[{
                label:'男',
                value:0
              },{
                label:'女',
                value:1
              },{
                label:'未知',
                value:''
              }]
            }
          ]
        }
      }
    }
}
</script>

```
:::



## 空心样式
:::demo  配置`border`为`true`
```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
export default {
    data() {
      return {
        form: {
          checkbox:[0,1]
        },
        option: {
          column: [{
              label: '空心多选',
              prop: 'checkbox',
              span:24,
              type: 'checkbox',
              border:true,
              dicData:[{
                label:'男',
                value:0
              },{
                label:'女',
                value:1
              },{
                label:'未知',
                value:''
              }]
            }
          ]
        }
      }
    }
}
</script>

```
:::
