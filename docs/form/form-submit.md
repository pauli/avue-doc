# 防重提交
提交后会禁止表单和按钮防止重复提交

## 
:::demo  `submit`中调用`done`方法和`crud`中使用一致
```html
<avue-form :option="option" v-model="form" @submit="handleSubmit"></avue-form>
<script>
export default {
  data(){
    return {
       form:{
          text:'动态内容1'
       },
       option:{
          column: [{
              label: "用户名",
              prop: "username"
          }]
       }
    }
  },
  methods:{
    handleSubmit(form,done){
       this.$message.success('3s后关闭');
       setTimeout(()=>{
          done()
       },3000)
    }
  }
}
</script>

```
:::

