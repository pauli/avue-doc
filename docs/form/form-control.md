# 与其它字段交互
:::tip
 2.8.10+
::::

## 普通用法
:::demo  可以写判断逻辑，返回对应改变的对象属性
```html
<avue-form :option="option"  v-model="form"></avue-form>
<script>
export default {
  data(){
    return {
       form:{
          text1:0,
       },
       option:{
          column: [{
            label: '内容1',
            prop: 'text1',
            type:'radio',
            control:(val,form)=>{
              if(val===0){
                return {
                  text2:{
                    display:true
                  },
                  text3:{
                    label:'内容3'
                  }
                }
              }else{
                return {
                  text2:{
                    display:false
                  },
                  text3:{
                    label:'有没有发现我变了'
                  }
                }
              }
            },
            dicData:[{
              label:'显示',
              value:0
            },{
              label:'隐藏',
              value:1,
            }]
          },{
            label: '内容2',
            prop: 'text2',
            display:true
          },{
            label: '内容3',
            prop: 'text3'
          }]
       }
    }
  }
}
</script>

```
:::

