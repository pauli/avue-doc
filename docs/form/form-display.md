# 隐藏字段

:::demo  对应的字段配置`display`为`false`即可隐藏
```html
<avue-form :option="option" v-model="form"></avue-form>
<script>
export default {
    data() {
      return {
        form: {},
        option: {
        column: [{
              label: "姓名",
              prop: "name",
              span:8
          },
          {
              label: "密码",
              prop: "password",
              type:'password',
              display:false
          }]
        }
      }
    }
}
</script>

```
:::