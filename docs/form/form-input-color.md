# 颜色选择器
:::tip
 1.0.10+
::::

## 普通用法 
:::demo  
```html
<avue-form :option="option" v-model="form" > </avue-form>

<script>
export default{
  data() {
    return {
      form: { 
        color:'rgba(42, 20, 20, 1)',
      },
      option: {
        column: [
          {
            label: '颜色选择器',
            prop: 'color',
            type: 'color',
          }
        ]
      }
    }
  }
}
</script>

```
:::



## 颜色格式
:::demo  
```html
<avue-form :option="option" v-model="form" > </avue-form>

<script>
export default{
  data() {
    return {
      form: { 
        color:'#3E1414',
      },
      option: {
        column: [
          {
            label: '颜色选择器',
            prop: 'color',
            type: 'color',
            colorFormat:"hex",
            showAlpha:false
          }
        ]
      }
    }
  }
}
</script>

```
:::