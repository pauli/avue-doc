module.exports = {
  lang: 'zh-CN',
  title: 'Avue',
  description: '前端搬砖神器,让数据驱动视图,更加贴合企业开发',
  themeConfig: {
    lastUpdated: 'Last Updated',
    logo: '/images/logo.png',
    nav: [{
      text: '首页',
      link: '/',
    }, {
      text: '文档',
      link: '/doc',
      items: [{
        text: '开发指南',
        link: '/docs/installation.md'
      }, {
        text: '单组件',
        link: '/component/input.md'
      }, {
        text: 'Crud/Form属性文档',
        link: '/views/doc'
      }, {
        text: 'Form组件',
        link: '/form/form.md'
      }, {
        text: 'Crud组件',
        link: '/crud/crud.md'
      }, {
        text: 'Data组件',
        link: '/data/data0.md'
      }, {
        text: 'Default其它组件',
        link: '/default/xlsx.md'
      }]
    },
    {
      text: '企业授权',
      items: [{
        text: '企业版',
        link: '/views/vip.md',
      }, {
        text: '授权查询',
        link: '/views/license.md'
      }]
    }, {
      text: '物料市场',
      items: [{
        text: '周边生态',
        link: '/plugins/avue-cli'
      }, {
        text: 'Avue Cloud公众号',
        link: '/views/article.md'
      }]
    },
    {
      text: '赞助支持',
      link: '/views/suport.md',
    },
    {
      text: '服务器1折起',
      items: [
        {
          text: '新用户专项优惠',
          link: 'https://promotion.aliyun.com/ntms/yunparter/invite.html?userCode=vqed4m0j',
        }, {
          text: '企业专项优惠',
          link: 'https://chuangke.aliyun.com/special/88?taskCode=shuangchuang_88_lb&recordId=876642&userCode=vqed4m0j',
        }]
    }, {
      text: '个人支付接口',
      link: 'https://www.yungouos.com/#/invite/welcome?spm=MTA0MTc=',
    }],
    sidebar: {
      '/component': [
        "/component/input",
        "/component/input-tree",
        "/component/input-number",
        "/component/input-color",
        "/component/input-icon",
        "/component/input-map",
        "/component/input-table",
        "/component/array",
        "/component/img",
        "/component/url",
        "/component/select",
        "/component/radio",
        "/component/checkbox",
        "/component/switch",
        "/component/datetime",
        "/component/time",
        "/component/rate",
        "/component/slider",
        "/component/cascader"
      ],
      '/plugins': [
        '/plugins/avue-cli',
        '/plugins/ueditor-plugins',
        '/plugins/print-plugins',
        '/plugins/excel-plugins',
        '/plugins/map-plugins',
        { title: "Data大屏设计器", path: 'https://data.avuejs.com' },
        { title: "Crud设计器", path: 'https://crud.avuejs.com' },
        { title: "Form设计器", path: 'https://form.avuejs.com' }
      ],
      '/form': [
        "/form/form",
        "/form/form-dialog",
        "/form/form-label",
        "/form/form-display",
        "/form/form-slot",
        "/form/form-value",
        "/form/form-rules",
        "/form/form-dic",
        "/form/form-dataType",
        "/form/form-filter",
        "/form/form-submit",
        "/form/form-bind",
        "/form/form-order",
        "/form/form-component",
        "/form/form-event",
        "/form/form-gutter",
        "/form/form-ref",
        "/form/form-detail",
        "/form/form-group",
        "/form/form-tabs",
        "/form/form-control",
        "/form/form-params",
        "/form/form-mock",
        "/form/form-tip",
        "/form/form-ajax",
        "/form/form-input",
        "/form/form-number",
        "/form/form-select",
        "/form/form-cascader",
        "/form/form-checkbox",
        "/form/form-radio",
        "/form/form-date",
        "/form/form-time",
        "/form/form-upload",
        "/form/form-dynamic",
        "/form/form-input-tree",
        "/form/form-input-icon",
        "/form/form-input-table",
        "/form/form-input-map",
        "/form/form-input-color",
        "/form/form-search",
        "/form/form-rate",
        "/form/form-slider"
      ],
      '/crud': [
        "/crud/crud",
        "/crud/crud-page",
        "/crud/crud-search",
        "/crud/crud-head",
        "/crud/crud-row",
        "/crud/crud-column",
        "/crud/crud-menu",
        "/crud/crud-fun",
        "/crud/crud-text",
        "/crud/crud-btn-slot",
        "/crud/crud-form",
        "/crud/crud-bind",
        "/crud/crud-dic",
        "/crud/crud-sum",
        "/crud/crud-export",
        "/crud/crud-tree",
        "/crud/crud-children",
        "/crud/crud-cell",
        "/crud/crud-ajax",
        "/crud/crud-permission",
        "/crud/crud-rc",
        "/crud/crud-empty",
        "/crud/crud-sortable",
        "/crud/crud-default",
        "/crud/crud-reload",
        "/crud/crud-bigcousin",
        '/crud/api-crud-big',
        '/crud/api-crud-temp',
        '/crud/api-crud-fun',
      ],
      '/data': [
        '/data/data0',
        '/data/data1',
        '/data/data2',
        '/data/data3',
        '/data/data4',
        '/data/data5',
        '/data/data6',
        '/data/data7',
        '/data/data8',
        '/data/data9',
        '/data/data10',
        '/data/data11',
        '/data/data12',
      ],
      '/default': [
        "/default/xlsx",
        "/default/export",
        "/default/login",
        "/default/keyboard",
        "/default/notice",
        "/default/screenshot",
        "/default/video",
        "/default/verify",
        "/default/print",
        "/default/nprogress",
        "/default/chat",
        "/default/license",
        "/default/sign",
        "/default/flow",
        "/default/skeleton",
        "/default/article",
        "/default/comment",
        "/default/code",
        "/default/count-up",
        "/default/card",
        "/default/draggable",
        "/default/dialog-form",
        "/default/image-preview",
        "/default/clipboard",
        "/default/watermark",
        "/default/empty",
        "/default/text-ellipsis",
        "/default/avatar",
        "/default/affix",
        "/default/search",
        "/default/tabs",
        "/default/tree"
      ],
      '/docs': [
        '/docs/home',
        '/docs/changelog',
        '/docs/contribution',
        '/docs/installation',
        '/docs/global',
        '/docs/locale',
        '/docs/api',
        '/docs/datatype',
        '/docs/vip',
        '/docs/pirate'
      ]
    },

  },
  chainWebpack: (config) => {
    config.resolve.alias.set('core-js/library/fn', 'core-js/features')
    config.globalObject = 'this'
  },
  plugins: ['demo-container', '@vuepress/back-to-top', '@vuepress/nprogress', {
    name: "notice-plugin",
    globalUIComponents: ["Notice"],
  }]
}

