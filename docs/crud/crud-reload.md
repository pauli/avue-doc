# 表格初始化


:::tip
- 如果表格出现错位等其它错误
- 可以参考例子利用key初始化一下组件
::::

:::demo 也可以调用内置方法`refreshTable`去初始化
```html
<el-button type="primary" size="small" @click="handleReload" >Key初始化</el-button>
<el-button type="primary" size="small" @click="handleReload1" >内置方法初始化</el-button>
<br /><br />
<avue-crud :key="reload" ref="crud" :data="data" :option="option"></avue-crud>

<script>
export default {
    data() {
      return {
        reload: Math.random(),
        data: [
          {
            username:'smallwei',
            password:'smallwei'
          }, {
            username:'avue',
            password:'avue'
          }
        ],
        option:{
          column:[
             {
              label:'用户名',
              prop:'username'
            }, {
              label:'密码',
              prop:'password',
              type:'password'
            }
          ]
        },
      }
    },
    methods: {
      handleReload(){
        this.reload=Math.random();
        this.$message.success('初始化完成')
      },
      handleReload1(){
        this.$refs.crud.refreshTable()
        this.$message.success('初始化完成')
      }
    }
}
</script>
```
:::
