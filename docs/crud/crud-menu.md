# 操作栏配置


## 操作栏隐藏

:::demo `menu`属性接受一个`Boolean`的属性达到隐藏操作栏的效果，默认为`false`
```html
<avue-crud :data="data" :option="option1"></avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option1:{
          menu:false,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    }
}
</script>
```
:::

## 操作栏宽度和其它属性

:::demo `menuWidth`属性接受一个值
```html
<avue-crud :data="data" :option="option1"></avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option1:{
          menuWidth:100,
          menuAlign:'center',
          menuHeaderAlign:'center',
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    }
}
</script>
```
:::


## 查看按钮
:::tip
 [自定义按钮](/crud/crud-btn-slot.html#自定义查看按钮)
::::

:::demo  `viewBtn`配置为`true`即可
```html
<avue-crud ref="crud" :option="option" :data="data"></avue-crud>
<script>
export default {
  data(){
    return {
       data:[
         {
           name:'张三',
           age:12,
           address:'码云的地址'
         }, {
           name:'李四',
           age:13,
           address:'码云的地址'
         }
       ],
       option:{
          viewBtn:true,
          editBtn:false,
          delBtn:false,
          column: [{
            label: '姓名',
            prop: 'name'
          },{
            label: '年龄',
            prop: 'age'
          },{
            label:'地址',
            span:24,
            prop:'address',
            type:'textarea'
          }]
       }
    }
  }
}
</script>

```
:::


## 复制按钮
:::tip
 2.6.14+
::::

:::demo 设置`copyBtn`为`true`时激活行复制功能,复制的数据会去除`rowKey`配置的主键
```html

{{form}}
<avue-crud :data="data" :option="option" v-model="form"></avue-crud>

<script>
export default {
    data() {
      return {
        form:{},
        data: [
          {
            ids:1,
            name:'张三',
            sex:'男'
          }, {
            ids:2,
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          rowKey:'ids',
          copyBtn:true,
          delBtn:false,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    }
}
</script>
```
:::

## 打印按钮

:::demo  `printBtn`设置为`true`即可开启打印功能
```html
<avue-crud :option="option" :data="data"></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       }],
       option:{
          menu:false,
          printBtn:true,
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  }
}
</script>

```
:::

## 导出按钮

:::demo  `excelBtn`设置为`true`即可开启打印功能
```html
<avue-crud :option="option" :data="data"></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       }],
       option:{
          menu:false,
          excelBtn:true,
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  }
}
</script>

```
:::


## 筛选按钮

:::tip
 常用自定筛选条件
::::

:::demo  `filterBtn`默认为`true`，可以自定义过滤条件，根据`filter`函数回调
```html
<avue-crud :option="option" :data="data" @filter="filterChange"></avue-crud>
<script>
export default {
  data(){
    return {
       data:[{
          text1:'内容1-1',
          text2:'内容1-2'
       },{
          text1:'内容2-1',
          text2:'内容2-2'
       }],
       option:{
          filterBtn:true,
          align:'center',
          column: [{
            label: '列内容1',
            prop: 'text1',
          }, {
            label: '列内容2',
            prop: 'text2',
          }]
       }
    }
  },
  methods:{
    filterChange(result) {
      this.$message.success('过滤器' + JSON.stringify(result))
    },
  }
}
</script>

```
:::




## 自定义操作栏

:::demo  有时候我们自定义按钮，操作栏宽度不够，它是不能自适应的,需要设置`menuWidth`属性去改变宽度，同时在`menu`名字的卡槽接受我们自定义的`dom`，返回的`scope`中返回了当前行`row`以及其他我们需要的数据
```html
<avue-crud :data="data" :option="option">
  <template slot-scope="{type,size}" slot="menu">
     <el-button icon="el-icon-check" :size="size" :type="type">自定义菜单按钮</el-button>
  </template>
</avue-crud>

<script>
export default {
    data() {
      return {
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }
        ],
        option:{
          menuWidth:380,
          column:[
             {
              label:'姓名',
              prop:'name'
            }, {
              label:'性别',
              prop:'sex'
            }
          ]
        },
      };
    }
}
</script>
```
:::

