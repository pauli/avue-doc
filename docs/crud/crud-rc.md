# 动态行列合并

:::demo 

```html
<avue-crud
  :data="data"
  :option="option"
  :span-method="spanMethod"
></avue-crud>

<script>
  export default {
    data() {
      return {
        data: [
          {
            name: '王小虎',
            amount1: '234',
            amount2: '3.2',
            amount3: 10
          },
          {
            name: '王小虎',
            amount1: '165',
            amount2: '4.43',
            amount3: 12
          },
          {
            name: '王小虎',
            amount1: '324',
            amount2: '1.9',
            amount3: 9
          },
          {
            name: '王小虎',
            amount1: '621',
            amount2: '2.2',
            amount3: 17
          },
          {
            name: '王小虎',
            amount1: '539',
            amount2: '4.1',
            amount3: 15
          },
          {
            name: '王大虎',
            amount1: '539',
            amount2: '4.1',
            amount3: 15
          }
        ],
        option: {
          border: true,
          column: [
            {
              label: '姓名',
              prop: 'name'
            },
            {
              label: '数值 1',
              prop: 'amount1'
            },
            {
              label: '数值 2',
              prop: 'amount2'
            },
            {
              label: '数值 3',
              prop: 'amount3'
            }
          ]
        },
        spanArr:[],
        key:'name'
      }
    },
    created(){
        this.rowspan()
    },
    methods: {
      rowspan() {
       this.spanArr=[];
       this.position=0;
       this.data.forEach((item,index)=>{
         if(index===0){
           this.spanArr.push(1)
           this.position=0;
         }else{
           if(this.data[index][this.key]===this.data[index-1][this.key]){
             this.spanArr[this.position] +=1;
             this.spanArr.push(0)
           }else{
             this.spanArr.push(1)
             this.position=index
           }
         }
       })
      },
      spanMethod({ row, column, rowIndex, columnIndex }) {
        if(column.property==[this.key]){
          const _row=this.spanArr[rowIndex];
          const _col=_row>0?1:0;
          return {
            rowspan:_row,
            colspan:_col
          }
        }
        
      }
    }
  }
</script>
```

:::