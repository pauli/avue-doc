# 基础

:::tip
- 表格中打开表单配置属性参考[弹窗表单配置项](/crud/crud-form)
- 表格的行配置参考[表格行配置项](/crud/crud-row.html)
- 表格的列配置项目参考[表格列配置项](/crud/crud-column.html)
- 表格的操作栏自定义卡槽的参考[自定义按钮](/crud/crud-btn-slot.html)
::::

:::demo  `data`数据的对象数组，`option`为表格要配置的数据列
```html
<el-row style="margin-bottom:20px;font-size:15px">
 <el-col :span="4">等待加载框: <el-switch size="small" v-model="showLoading"> </el-switch></el-col>
  <el-col :span="4">显示边框: <el-switch size="small" v-model="showBorder"> </el-switch></el-col>
  <el-col :span="4">显示斑马纹: <el-switch size="small" v-model="showStripe"> </el-switch></el-col>
  <el-col :span="4">显示索引: <el-switch size="small" v-model="showIndex"> </el-switch></el-col>
  <el-col :span="4">显示多选框: <el-switch size="small" v-model="showCheckbox"> </el-switch></el-col>
  <el-col :span="4">显示表头: <el-switch size="small" v-model="showHeader"> </el-switch></el-col>
  <el-col :span="4">显示分页: <el-switch size="small" v-model="showPage"> </el-switch></el-col>
  <el-col :span="4">卡片模式: <el-switch size="small" v-model="showCard"> </el-switch></el-col>
</el-row>
<el-row style="margin-bottom:20px">
  类型：<el-radio-group v-model="menuType" size="small">
      <el-radio-button label="text">默认</el-radio-button>
      <el-radio-button label="icon">菜单按钮</el-radio-button>
      <el-radio-button label="text">文本按钮</el-radio-button>
      <el-radio-button label="menu">合并菜单</el-radio-button>
    </el-radio-group>
</el-row>
<el-row style="margin-bottom:20px">
   大小：<el-radio-group v-model="sizeValue" size="small">
    <el-radio-button label="small">默认</el-radio-button>
    <el-radio-button label="medium">medium</el-radio-button>
    <el-radio-button label="small">small</el-radio-button>
    <el-radio-button label="mini">mini</el-radio-button>
  </el-radio-group>
</el-row>
<avue-crud :data="data" :option="option" :table-loading="showLoading" :page.sync="page">
    <template slot-scope="scope" slot="menuBtn">
      <el-dropdown-item divided >自定义按钮</el-dropdown-item>
    </template>
    <template slot-scope="{type,size}" slot="menu">
      <el-button :type="type" :size="size">自定义按钮</el-button>
    </template>
</avue-crud>

<script>
export default {
 data() {
      return {
        page:{
          total:100,
          currentPage: 1
        },
        data: [
          {
            name:'张三',
            sex:'男'
          }, {
            name:'李四',
            sex:'女'
          }, {
            name:'王五',
            sex:'女'
          }, {
            name:'赵六',
            sex:'男'
          }
        ],
        menuType:'text',
        showLoading:false,
        showCard:false,
        showBorder: false,
        showStripe: false,
        showHeader: true,
        showIndex: true,
        showCheckbox: false,
        showPage:true,
        sizeValue:'small'
      }
    },
     computed: {
       option(){
         return{
            title:'表格的标题',
            titleSize:'h3',
            titleStyle:{
              color:'red'
            },
            card:this.showCard,
            border:this.showBorder,
            stripe:this.showStripe,
            showHeader:this.showHeader,
            index:this.showIndex,
            size:this.sizeValue,
            selection:this.showCheckbox,
            page:this.showPage,
            align:'center',
            menuAlign:'center',
            menuType:this.menuType,
            menuBtnTitle:'自定义名称',
            column:[
              {
                label:'姓名',
                prop:'name',
                search:true
              },
              {
                label:'性别',
                prop:'sex'
              }
            ]
         }
       }
     }
  }
</script>
```
:::
