# 全局配置
在引入 Avue 时，可以传入一个全局配置对象。该对象目前支持 
- size：用于改变组件的默认尺寸，属性的组件的默认尺寸均为 `small`。可选值`small`/`mini`/`medium`;
- menuType：用于改变操作栏菜单按钮类型性，属性的组件的默认尺寸均为 `text`。可选值`button`/`icon`/`text`/`menu`
- theme 主题颜色配置，属性的组件的默认白色。可选值 `dark`;
- qiniu 七牛云配置
``` javascript
  {
    AK: '',
    SK: '',
    scope: '',
    url: '',
    deadline: 1
  }
```
- ali 阿里云配置
``` javascript
  {
    region: '',
    endpoint: '',
    accessKeyId: '',
    accessKeySecret: '',
    bucket: '',
  }
```
- canvas全局水印配置
``` javascript
  {
    text: 'avuejs.com',
    fontFamily: 'microsoft yahei',
    color: "#999",
    fontSize: 16,
    opacity: 100,
    bottom: 10,
    right: 10,
    ratio: 1
  }
```
``` javascript
 const app = Vue.createApp({});
  app.use(AVUE,{
  size:'',
  menuType:'',
  qiniu:{},
  ali:{},
  theme:'',
  canvas:{}
});
```