# 贡献指南

### 介绍
感谢你使用 Avue。

以下是关于向 Avue 提交反馈或代码的指南。在向 Avue 提交 issue 或者 PR 之前，请先花几分钟时间阅读以下文字。


### Issue 规范
遇到问题时，请先确认这个问题是否已经在 issue 中有记录或者已被修复
提 issue 时，请用简短的语言描述遇到的问题，并添加出现问题时的环境和复现步骤


### 参与开发
##### 本地开发
按照下面的步骤操作，即可在本地开发 Avue 组件。

``` bash  
# 克隆仓库
# 开发分支为 dev 分支
git clone git@gitee.com:smallweigit/avue.git

# 安装依赖
cd Avue && yarn 或者 npm install

# 进入开发模式
npm run dev

```


### 目录结构
仓库的组件代码位于 packages 下，每个组件一个文件夹
src 为组件提供的核心方法等
项目主要目录如下：

```
Avue
├─ build     # 构建
├─ packages  # 组件
├─ src       # 方法
├─ lib       # 生成
└─ examples  # 测试
```



### 提交 PR

#### Pull Request 规范
- 如果遇到问题，建议保持你的 PR 足够小。保证一个 PR 只解决一个问题或只添加一个功能
- 当新增组件或者修改原有组件时，记得增加或者修改测试代码，保证代码的稳定
- 在 PR 中请添加合适的描述，并关联相关的 Issue

#### Pull Request 流程
- fork 主仓库，如果已经 fork 过，请同步主仓库的最新代码
- 基于 fork 后仓库的 dev 分支新建一个分支，比如feature/button_color
- 在新分支上进行开发，开发完成后，提 Pull Request 到主仓库的 dev 分支
- Pull Request 会在 Review 通过后被合并到dev分支
- 等待 Avue 发布版本，随缘更新

#### 同步最新代码
提 Pull Request 前，请依照下面的流程同步主仓库的最新代码：

``` bash
# 添加主仓库到 remote，作为 fork 后仓库的上游仓库
git remote add upstream https://gitee.com/smallweigit/avue.git

# 拉取主仓库最新代码
git fetch upstream

# 切换至 dev 分支
git checkout dev

# 合并主仓库代码
git merge upstream/dev
```