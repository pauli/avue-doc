# 企业须知

## 会员说明 (Membership notes)

付款后账号权限开通来登陆git私服下载代码

- 会员相关资料发送到购买的QQ邮箱
- 私服地址：[https://git.avuejs.com](https://git.avuejs.com)
- 会员群：[https://avuejs.com/images/qqqun.png](https://avuejs.com/images/qqqun.png)
- 说明：登录私服账号后请及时修改密码，以防盗用；请勿账号多用，借用，二次贩卖，如有异常，永久停封！！！


## 项目介绍 (Project introduction)

登录私服后你可以看到如下项目

### 项目
  - avue-data——数据大屏源码
  - avue-data-server——数据大屏后端(node版本)
  - avue-cli——后台模板
  - avue-form——表单设计生成器
  - avue-crud——表格设计生成器
### 文档
  - avue-kanyun——看云收费文档