# ImagePreview 图片预览
可以赋值任赋值图片去放大预览(一张缩略图，一张放大图)

:::tip
 2.5.3+
::::


:::demo 
```html
<div>
  <el-button @click="openPreview(0)" style="margin-bottom:20px;">打开图片预览</el-button>
  <p>
    <img width="200px" style="margin-right:20px" v-for="(d, index) of datas" :src="d.thumbUrl" @click="openPreview(index)">
  </p>
</div>
<script>
export default {
   data() {
    return {
      datas: [
        { thumbUrl: `https://img.alicdn.com/tfs/TB1uevcCrj1gK0jSZFuXXcrHpXa-1880-640.jpg`, url: `https://img.alicdn.com/tfs/TB1uevcCrj1gK0jSZFuXXcrHpXa-1880-640.jpg` },
        { thumbUrl: `https://img.alicdn.com/tfs/TB1v28TC8v0gK0jSZKbXXbK2FXa-1880-640.jpg`, url: `https://img.alicdn.com/tfs/TB1v28TC8v0gK0jSZKbXXbK2FXa-1880-640.jpg` },
      ]
    }
  },
  methods: {
    openPreview(index = 0) {
      this.$ImagePreview(this.datas, index,{
        closeOnClickModal: true,
        beforeClose:()=>{
          this.$message.success('关闭回调')
        }
      });
    }
  }
}
</script>
```
:::




