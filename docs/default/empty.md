# Empty 空状态

空状态时的展示占位图，当目前没有数据时，用于显式的用户提示。
:::tip
 1.0.8+
::::

## 普通用法 
:::demo 
```html
<avue-empty></avue-empty>
<script>
export default {
  
}
</script>

```
:::


## 调节大小 
:::demo 
```html
<avue-empty :size="40"></avue-empty>
<script>
export default {
  
}
</script>

```
:::


## 自定义用法
:::demo 
```html
<avue-empty
  image="https://gw.alipayobjects.com/mdn/miniapp_social/afts/img/A*pevERLJC9v0AAAAAAAAAAABjAQAAAQ/original"
  desc="自定义的提示语">
  <el-button type="primary">创建新东西</el-button>
</avue-empty>
<script>
export default {
  
}
</script>

```
:::

## Variables

|参数|说明|类型|可选值|默认值|
|-------------|-------------------------------------------------------------|--------|------|------|
|image|设置显示图片，为 string 时表示自定义图片地址|string|—|—|
|size|图片的大小|string|—|80|
|desc|自定义描述内容|string|—|暂无数据|

## Scoped Slot

|name|说明|
|---|-----|
|—|Empty 的内容|

