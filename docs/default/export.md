# Excel导出

:::tip
 2.0.3+
::::

``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里）-->
<script src="https://cdn.staticfile.org/FileSaver.js/2014-11-29/FileSaver.min.js"></script>
<script src="https://cdn.staticfile.org/xlsx/0.18.2/xlsx.full.min.js"></script>
```

:::demo 
```html
<div style="width:400px">
 <el-button type="primary" @click="handleExcel">下载 excel</el-button>
 <el-button type="success" @click="handleExcel1">下载 多级表头excel</el-button>
</div>
<script>
export default {
  data(){
    return {}
  },
  methods: {
     handleExcel() {
      let opt = {
        title: '文档标题',
        column: [{
          label: '标题',
          prop: 'title'
        }],
        data: [{
          title: "测试数据1"
        }, {
          title: "测试数据2"
        }]
      }
      this.$Export.excel({
        title: opt.title ,
        columns: opt.column,
        data: opt.data
      });
    },
    handleExcel1() {
      let opt = {
        title: '文档标题',
        column: [{
          label:'多级表头',
          prop:'header',
          children:[
            {
              label: '标题1',
              prop: 'title1'
            },{
              label: '标题2',
              prop: 'title2'
            }
          ]
        }],
        data: [{
          title1: "测试数据1",
          title2: "测试数据2"
        }, {
          title1: "测试数据2",
          title2: "测试数据2"
        }]
      }
      this.$Export.excel({
        title: opt.title,
        columns: opt.column,
        data: opt.data
      });
    }
  }
}
</script>

```
:::



## Variables

| 参数   | 说明   | 类型   | 可选值 | 默认值               |
| ------ | ------ | ------ | ------ | -------------------- |
| title  | 标题   | String | -      | new Date().getTime() |
| column | 数据列 | Array  | -      | -                    |
| data   | 数据   | Array  | -      | -                    |