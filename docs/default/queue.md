# 入场动画

```
<!-- 导入动画需要的包 -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />
<!-- 参考动画 -->
https://www.dowebok.com/demo/2014/98/
```


## 普通用法
:::demo  `enter`为入场动画的名称（参考animate官网 https://daneden.github.io/animate.css），`delay`为延迟时间
```html
<avue-queue enter="rotateInDownLeft">
  <div style="width:100px;height:100px;background-color:#409eff;margin-bottom:10px;"></div>
</avue-queue>
<avue-queue enter="rotateInDownRight">
  <div style="width:100px;height:100px;background-color:#409eff"></div>
</avue-queue>
<script>
export default {

}
</script>
```
:::


## 内嵌其它组件
:::demo  
```html
<avue-queue>
  <el-button type="success">按钮1</el-button>
</avue-queue>
</br></br>
<avue-queue enter="fadeInRight">
  <el-button type="danger">按钮2</el-button>
</avue-queue>
<script>
export default {

}
</script>
```
:::

